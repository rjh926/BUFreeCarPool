package bu.ac.kr.bufreecarpool.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import bu.ac.kr.bufreecarpool.R;

public class FinalActivity extends AppCompatActivity {

    public Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        exit = (Button) findViewById(R.id.exit);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(FinalActivity.this, MapsActivity.class);
                startActivity(back);
                finish();
            }
        });
    }
}
