package bu.ac.kr.bufreecarpool.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import bu.ac.kr.bufreecarpool.R;
import bu.ac.kr.bufreecarpool.domain.User;

public class JoinActivity extends AppCompatActivity {

     EditText jEmail;
     EditText jPassword;
    EditText jName;
     EditText jPhone;

    private Button completeBtn;
    private Button cancelBtn;
    SQLiteDatabase SqlDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        jEmail = (EditText) findViewById(R.id.joinEmail);
        jPassword = (EditText) findViewById(R.id.joinPassword);
        jName = (EditText) findViewById(R.id.joinName);
        jPhone = (EditText) findViewById(R.id.joinPhone);
        completeBtn = (Button)findViewById(R.id.Complete);
        cancelBtn = (Button) findViewById(R.id.cancel);

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = null;
                try {
                    user = new User();
                    user.setUserId(jEmail.getText().toString().trim());
                    user.setUserName(jName.getText().toString().trim());
                    user.setPassword(jPassword.getText().toString().trim());
                    user.setPhonNum(jPhone.getText().toString().trim());

                } catch (Exception e){
                    e.printStackTrace();
                }
                Intent back = new Intent();
                setResult(Activity.RESULT_OK,back);
                Toast.makeText(getApplicationContext(),user.getUserName(),Toast.LENGTH_SHORT).show();
                finish();


            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent canceledJoin = new Intent();
                setResult(Activity.RESULT_CANCELED,canceledJoin);
                Toast.makeText(getApplicationContext(),"회원가입이 취소되었습니다.",Toast.LENGTH_SHORT).show();
                finish();
            }
        });



    }
}