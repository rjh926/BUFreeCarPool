package bu.ac.kr.bufreecarpool.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import bu.ac.kr.bufreecarpool.R;

public class SelectActivity extends AppCompatActivity {

    public Button back;
    public Button next;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        back = (Button)findViewById(R.id.back);
        next = (Button)findViewById(R.id.next);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(SelectActivity.this,MapsActivity.class);
                startActivity(back);
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(SelectActivity.this,PaxActivity.class);
                startActivity(next);
                finish();
            }
        });
    }




}