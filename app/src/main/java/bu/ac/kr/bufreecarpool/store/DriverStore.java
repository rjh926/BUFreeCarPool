package bu.ac.kr.bufreecarpool.store;

import bu.ac.kr.bufreecarpool.domain.Driver;

public interface DriverStore {
    public boolean create(Driver driver);
    public boolean delete(Driver driver);
    public Driver readByCarNum(String carNum);
}
