package bu.ac.kr.bufreecarpool.service;

import bu.ac.kr.bufreecarpool.domain.Driver;

public interface DriverService {
    public boolean register(Driver driver);
    public boolean delete(Driver driver);
    public Driver retrieveByCarNum(String carNum);
}
