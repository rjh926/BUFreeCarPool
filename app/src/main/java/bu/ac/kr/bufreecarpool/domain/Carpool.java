package bu.ac.kr.bufreecarpool.domain;

import java.io.Serializable;

public class Carpool implements Serializable {

    private int applyNum ;
    private String userId;
    private String startPoint;
    private String destination;
    private String phoneNum;
    private int people;

    public void setPeople(int people) {
        this.people = people;
    }

    public int getPeople() {

        return people;
    }

    public void setApplyNum(int applyNum) {
        this.applyNum = applyNum;
    }

    public int getApplyNum() {

        return applyNum;
    }

    public String getUserId() {
        return this.userId;
    }
    public String getStartPoint(){
        return this.startPoint;
    }
    public String getDestination(){
        return this.destination;

    }
    public String getPhoneNum(){
        return  this.phoneNum;
    }

    public void setUserId(String id){
        this.userId = id;
    }
    public  void setStartPoint(String point){
        this.startPoint = point;

    }
    public void setDestination(String destination){
        this.destination = destination;
    }
    public void setPhoneNum(String phoneNum){
        this.phoneNum = phoneNum;
    }


}
