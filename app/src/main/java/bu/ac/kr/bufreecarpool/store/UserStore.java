package bu.ac.kr.bufreecarpool.store;

import java.util.List;

import bu.ac.kr.bufreecarpool.domain.User;

public interface UserStore {

    public void create(User user);
    public User read(User user);
    public void delete(User user);
    public void update(User user);
    public List<User> readAll();
}
