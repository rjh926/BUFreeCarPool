package bu.ac.kr.bufreecarpool.service;

import java.util.List;

import bu.ac.kr.bufreecarpool.domain.Carpool;

public interface ApplyService {
    public boolean register(Carpool carpool);
    public boolean remove(Carpool carpool);
    public List<Carpool> retrieveAll();
    public Carpool retrieveByDestination(String destination);
    public Carpool retrieveByStartPoin(String startPoint);
    public Carpool retrieveById(int ApplyId);
    public List<Carpool> retrieveByPeople(int people);
}
