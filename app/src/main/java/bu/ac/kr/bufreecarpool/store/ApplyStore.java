package bu.ac.kr.bufreecarpool.store;

import java.util.List;

import bu.ac.kr.bufreecarpool.domain.Carpool;

public interface ApplyStore {
    public boolean create(Carpool carpool);
    public boolean delete(Carpool carpool);
    public Carpool readById(int applyId);
    public List<Carpool> readAll();
    public  List<Carpool> readByDestination(String destination);
    public List<Carpool> readByStartPoin(String startPoint);
    public List<Carpool> readByPeople(int people);
}
