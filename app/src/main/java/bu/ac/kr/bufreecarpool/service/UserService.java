package bu.ac.kr.bufreecarpool.service;

import bu.ac.kr.bufreecarpool.domain.User;

public interface UserService {
    public boolean register(User user);
    public boolean delete(User user);
    public User read(User user);
    public User login(User user);

}
