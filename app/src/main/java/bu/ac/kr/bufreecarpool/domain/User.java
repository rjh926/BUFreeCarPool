package bu.ac.kr.bufreecarpool.domain;

import java.io.Serializable;

public class User implements Serializable {

    private String userId;
    private String password;
    private String userName;
    private String phonNum;

    public String getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhonNum() {
        return phonNum;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPhonNum(String phonNum) {
        this.phonNum = phonNum;
    }

}
