package bu.ac.kr.bufreecarpool.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import bu.ac.kr.bufreecarpool.R;
import bu.ac.kr.bufreecarpool.domain.User;
public class LoginActivity extends AppCompatActivity {

    private Button loginBtn = null;
    private Button joinBtn = null;
    private EditText email ;
    private EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginBtn = (Button)findViewById(R.id.loginbtn);
        joinBtn = (Button)findViewById(R.id.joinbtn);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = email.getText().toString();
                String pw = password.getText().toString();

                User user = new User();

                user.setUserId(id);
                user.setPassword(pw);

////

//               Toast.makeText(getApplicationContext(),reusult.getUserName(),Toast.LENGTH_SHORT).show();
//

               //  임시 하드코딩
                if(user.getUserId().equals("admin")){
                    if (user.getPassword().equals("1234")){

                        user.setUserName("RyouJaehyung");
                        Intent login = new Intent(LoginActivity.this,MapsActivity.class);

                        login.putExtra("User",user);
                        startActivity(login);
                        finish();

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"비밀번호가 틀렸습니다.",Toast.LENGTH_SHORT).show();
                    }
                }
                else {

                    Toast.makeText(getApplicationContext(),"ID가 틀렸습니다.", Toast.LENGTH_SHORT).show();
                }



            }
        });

        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent join = new Intent(LoginActivity.this,JoinActivity.class);
                startActivity(join);

            }
        });



    }
}
