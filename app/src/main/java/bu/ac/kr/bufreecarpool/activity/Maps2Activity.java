package bu.ac.kr.bufreecarpool.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import bu.ac.kr.bufreecarpool.R;

public class Maps2Activity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;

    Button location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final Marker selectedMarker;

        // Add a marker in Sydney and move the camera
        LatLng BU = new LatLng(36.839235, 127.184150);
        //mMap.addMarker(new MarkerOptions().position(BU).title("Marker in BU"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BU, 15));
        mMap.addMarker(new MarkerOptions().position(BU).title("나도 탈래요! 도착지:야우리 1명")).showInfoWindow();
        mMap.setOnInfoWindowClickListener(this);
    }

    public void onInfoWindowClick(Marker marker) {
        Intent i = new Intent(Maps2Activity.this,middle.class);
        startActivity(i);
        finish();
    }


}