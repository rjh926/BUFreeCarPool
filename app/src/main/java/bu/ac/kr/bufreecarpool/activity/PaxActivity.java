package bu.ac.kr.bufreecarpool.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import bu.ac.kr.bufreecarpool.R;

public class PaxActivity extends AppCompatActivity {

    public Button back;
    public Button next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pax);

        back = (Button) findViewById(R.id.back);
        next = (Button) findViewById(R.id.next);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(PaxActivity.this, SelectActivity.class);
                startActivity(back);
                finish();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(PaxActivity.this,Maps2Activity.class);
                startActivity(next);
                finish();
            }
        });

    }

    }

