package bu.ac.kr.bufreecarpool.domain;

import java.io.Serializable;

public class Driver implements Serializable {
    String id;
    String name;
    String phoneNum;
    String carModel;
    String carNumber;
    String carRegister;
    String insurance;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public void setCarRegister(String carRegister) {
        this.carRegister = carRegister;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getId() {

        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getCarRegister() {
        return carRegister;
    }

    public String getInsurance() {
        return insurance;
    }
}
